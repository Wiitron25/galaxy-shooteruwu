﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Instanciar_Nave : MonoBehaviour
{
    // Start is called before the first frame update
    public GameObject enemyshipPrefab;

    public float Time_stop;

    private float currentTime = 0;
    

    // Update is called once per frame
   
    void Update()
    {
        currentTime += Time.deltaTime;
        
        if (currentTime>Time_stop){
            currentTime = 0;
            Instantiate(enemyshipPrefab,new Vector3(9f,Random.Range(-5,5),0), Quaternion.identity, this.transform);
        }
    }
}