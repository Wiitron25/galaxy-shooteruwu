﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    public bool canTripleShot = false;
    //[SerializeField]
    public GameObject laserPrefab;
    public GameObject Triple_Laser;
    private float speed = 5;
    public float fireRate = 0.25f;
    public float canFire = 0.0f;


    void Start()
    {
        transform.position = new Vector3(0f, 0f,0f);
        
    }

    
    void Update()
    {
        Movement();
        if(Input.GetKeyDown(KeyCode.Space) || Input.GetMouseButtonDown(0))
        {
            Shoot();
        }
        
    }
    private void Shoot()
    {
        if (Time.time >canFire)
        {
           
            if (canTripleShot==true){
                Instantiate(Triple_Laser, transform.position + new Vector3(0,0, 0), Quaternion.identity);
                

            }
            else
            {
                Instantiate(laserPrefab, transform.position + new Vector3(0, 0.81f, 0), Quaternion.identity);
            }
            
        }
        
    }
    private void Movement()
    {
        float horizontalInput = Input.GetAxis("Horizontal");
        float verticalInput = Input.GetAxis("Vertical");

        transform.Translate(Vector3.right * Time.deltaTime * speed * horizontalInput);
        transform.Translate(Vector3.up * Time.deltaTime * speed * verticalInput);
        //Limites de la pantalla de la nave (Player)
        //Limites Verticales
        if (transform.position.y > 0)
        {
            transform.position = new Vector3(transform.position.x, 0, 0);
        }
        else if (transform.position.y < -4f)
        {
            transform.position = new Vector3(transform.position.x, -4f, 0);
        }
        //Limites Horizontales
        else if (transform.position.x > 9.5f)
        {
            transform.position = new Vector3(-9.5f, transform.position.y, 0);
        }
        else if (transform.position.x < -9.5f)
        {
            transform.position = new Vector3(9.5f, transform.position.y, 0);
        }
    }
}
